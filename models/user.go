package models

type User struct {
	Id                int    `json:"id"`
	Email             string `json:"email"`
	EncryptedPassword string `json:"-"`
}

func (u *User) Validate() {

}
