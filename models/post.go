package models

import (
	"encoding/json"
	"errors"
)

type Post struct {
	Id   int    `json:"id"`
	Text string `json:"text"`
	User User   `json:"user"`
}

func (post *Post) Validate() error {
	allErrors := []string{}

	if len(post.Text) < 5 {
		allErrors = append(allErrors, "Length of text is to small, minimum is 5")
	}
	if len(post.Text) > 100 {
		allErrors = append(allErrors, "Length of text is to big, maximum is 100")
	}

	if len(allErrors) != 0 {
		jsonbyte, err := json.Marshal(allErrors)
		if err != nil {
			return err
		}
		return errors.New(string(jsonbyte))
	}
	return nil
}
