CREATE TABLE users (
  Id SERIAL PRIMARY KEY,
  Email VARCHAR,
  Encrypted_Password VARCHAR
);