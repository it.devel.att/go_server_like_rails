package storages

import (
	"context"
	"log"

	"gitlab.com/it.devel.att/gominds/models"
)

type UserRepository struct {
	store *Store
}

func (u *UserRepository) Create(user *models.User) error {
	err := u.store.Open()
	if err != nil {
		log.Println("u.store.Open", err)
		return err
	}
	log.Printf("INSERT INTO users(email, encrypted_password) VALUES (%v, \"[PASSWORD]\") RETURNING id \n", user.Email)
	err = u.store.db.QueryRow(context.Background(), "INSERT INTO users(email, encrypted_password) VALUES ($1, $2) RETURNING id",
		&user.Email,
		&user.EncryptedPassword).Scan(&user.Id)
	if err != nil {
		log.Println("u.store.db.QueryRow", err)
		return err
	}
	defer u.store.Close()
	return nil
}

func (u *UserRepository) FindByEmail(email string, user *models.User) error {
	err := u.store.Open()
	if err != nil {
		log.Println("u.store.Open", err)
		return err
	}
	log.Printf("SELECT id, email, encrypted_password FROM users WHERE email = %v \n", email)
	err = u.store.db.QueryRow(context.Background(), "SELECT id, email, encrypted_password FROM users WHERE email = $1",
		email).Scan(&user.Id, &user.Email, &user.EncryptedPassword)
	if err != nil {
		log.Println("u.store.db.QueryRow", err)
		return err
	}
	defer u.store.Close()
	return nil
}
