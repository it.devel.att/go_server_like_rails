package storages

import (
	"context"
	"log"

	"gitlab.com/it.devel.att/gominds/models"
)

type PostRepository struct {
	store *Store
}

func (r *PostRepository) All(posts *[]models.Post) error {
	dbRequest := "SELECT id, text, user_id, (SELECT email FROM users WHERE id = user_id) FROM posts ORDER BY id DESC"
	err := r.store.Open()
	if err != nil {
		log.Println("r.store.Open", err)
		return err
	}
	rows, err := r.store.db.Query(context.Background(), dbRequest)
	if err != nil {
		log.Println("r.store.db.Query", err)
		return err
	}
	log.Println(dbRequest)
	defer rows.Close()

	for rows.Next() {
		post := models.Post{}
		err = rows.Scan(&post.Id, &post.Text, &post.User.Id, &post.User.Email)
		if err != nil {
			log.Println("rows.Scan", err)
			return err
		}
		*posts = append(*posts, post)
	}
	defer r.store.Close()
	return nil
}

func (r *PostRepository) Create(post *models.Post) error {
	err := r.store.Open()
	if err != nil {
		log.Println("r.store.Open", err)
		return err
	}
	log.Printf("INSERT INTO posts(text, user_id) VALUES (%v, %v) RETURNING id", post.Text, post.User.Id)
	err = r.store.db.QueryRow(context.Background(), "INSERT INTO posts(text, user_id) VALUES ($1, $2) RETURNING id",
		&post.Text,
		&post.User.Id).Scan(&post.Id)
	if err != nil {
		return err
	}
	defer r.store.Close()
	return nil
}

func (r *PostRepository) Find(id string, post *models.Post) error {
	err := r.store.Open()
	if err != nil {
		return err
	}
	err = r.store.db.QueryRow(
		context.Background(),
		"SELECT id, text, user_id, (SELECT email FROM users WHERE id = user_id) FROM posts WHERE id = $1",
		id).Scan(&post.Id, &post.Text, &post.User.Id, &post.User.Email)

	if err != nil {
		return err
	}
	defer r.store.Close()
	return nil
}

func (r *PostRepository) Update(post *models.Post) error {
	err := r.store.Open()
	if err != nil {
		return err
	}

	_, err = r.store.db.Exec(context.Background(), "UPDATE posts SET text = $1 WHERE id = $2",
		&post.Text,
		&post.Id)
	if err != nil {
		log.Println("r.store.db.Exec", err)
		return err
	}
	defer r.store.Close()
	return nil
}
