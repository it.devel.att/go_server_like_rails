package storages

import (
	"context"

	"github.com/jackc/pgx/v4"
)

type Store struct {
	db             *pgx.Conn
	postRepository *PostRepository
	userRepository *UserRepository
}

func (s *Store) Open() error {
	conn, err := pgx.Connect(context.Background(), "postgres://yaroslav:root@localhost:5432/gomind")
	if err != nil {
		return err
	}

	if err = conn.Ping(context.Background()); err != nil {
		return err
	}

	s.db = conn
	return nil
}

func (s *Store) Close() {
	s.db.Close(context.Background())
}

func (s *Store) Post() *PostRepository {
	if s.postRepository != nil {
		return s.postRepository
	}
	s.postRepository = &PostRepository{
		store: s,
	}
	return s.postRepository
}

func (s *Store) User() *UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}
	s.userRepository = &UserRepository{
		store: s,
	}
	return s.userRepository
}
