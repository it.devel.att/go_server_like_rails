package main

import (
	"log"
	"net/http"

	"gitlab.com/it.devel.att/gominds/config"
)

func main() {
	log.Print("Server starts at :9000")
	http.ListenAndServe(":9000", config.Routes())
}
