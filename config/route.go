package config

import (
	"github.com/julienschmidt/httprouter"
	. "gitlab.com/it.devel.att/gominds/controllers"
)

var postsController = PostsController{}
var usersController = UsersController{}
var sessionsController = SessionsController{}

func Routes() *httprouter.Router {
	router := httprouter.New()
	router.GET("/api/v1/posts/", postsController.Index)
	router.GET("/api/v1/posts/:id", postsController.Show)
	router.POST("/api/v1/posts/", postsController.Create)
	router.OPTIONS("/api/v1/posts/", postsController.Create)
	router.PUT("/api/v1/posts/:id", postsController.Update)
	router.OPTIONS("/api/v1/posts/:id", postsController.Update)

	router.POST("/api/v1/users/", usersController.Create)
	router.OPTIONS("/api/v1/users/", usersController.Create)

	router.POST("/api/v1/sessions/", sessionsController.Create)
	router.OPTIONS("/api/v1/sessions/", sessionsController.Create)

	return router
}
