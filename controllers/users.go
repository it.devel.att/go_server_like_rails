package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/it.devel.att/gominds/models"
	"golang.org/x/crypto/bcrypt"
)

type UsersController struct{}

type requestUserCreate struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"passwordConfirmation"`
}

func (u UsersController) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setupResponse(w)
	log.Println(r.Proto, r.Method, r.Host, r.URL)
	if r.Method == "OPTIONS" {
		return
	}

	requestUser := requestUserCreate{}
	err := readBodyFromParamsForUserCreate(&requestUser, r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if requestUser.Password != requestUser.PasswordConfirmation {
		http.Error(w, "Неверное подтверждение пароля", http.StatusBadRequest)
		return
	}

	secretPassword := []byte(requestUser.Password)
	hashedPassword, err := bcrypt.GenerateFromPassword(secretPassword, bcrypt.DefaultCost)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user := models.User{Email: requestUser.Email, EncryptedPassword: string(hashedPassword)}

	err = storage.User().Create(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write(json)
}

func readBodyFromParamsForUserCreate(reqUser *requestUserCreate, r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&reqUser)
	if err != nil {
		return err
	}
	return nil
}

func readBodyFromParamsForUser(user *models.User, r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		return err
	}
	return nil
}
