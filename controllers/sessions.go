package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/it.devel.att/gominds/models"
	"gitlab.com/it.devel.att/gominds/utils"
	"golang.org/x/crypto/bcrypt"
)

type SessionsController struct{}

type requestUserLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (s SessionsController) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setupResponse(w)
	log.Println(r.Proto, r.Method, r.Host, r.URL)
	if r.Method == "OPTIONS" {
		return
	}

	requestUser := requestUserLogin{}
	err := readBodyFromParamsForUserLogin(&requestUser, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user := models.User{}
	err = storage.User().FindByEmail(requestUser.Email, &user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if bcrypt.CompareHashAndPassword([]byte(user.EncryptedPassword), []byte(requestUser.Password)) != nil {
		log.Printf("Fail bcrypt.CompareHashAndPassword for %v\n", user.Email)
		http.Error(w, "Authorize error", http.StatusBadRequest)
		return
	}

	tokenJson, err := utils.CreateToken(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json, err := json.Marshal(tokenJson)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write(json)
}

func readBodyFromParamsForUserLogin(reqUser *requestUserLogin, r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&reqUser)
	if err != nil {
		return err
	}
	return nil
}
