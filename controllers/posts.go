package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/it.devel.att/gominds/models"
	"gitlab.com/it.devel.att/gominds/storages"
	"gitlab.com/it.devel.att/gominds/utils"
)

type PostsController struct{}

var storage = storages.Store{}

func (p PostsController) Index(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setupResponse(w)
	log.Println(r.Proto, r.Method, r.Host, r.URL)
	posts := []models.Post{}
	err := storage.Post().All(&posts)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	json, err := json.Marshal(posts)
	if err != nil {
		log.Println("json.Marshal(posts)", err)
		return
	}
	w.Write(json)
}

func (p PostsController) Show(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setupResponse(w)
	log.Println(r.Proto, r.Method, r.Host, r.URL)

	post := models.Post{}

	err := storage.Post().Find(ps.ByName("id"), &post)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	json, err := json.Marshal(post)
	if err != nil {
		log.Println(err)
		return
	}
	w.Write(json)
}

func (p PostsController) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setupResponse(w)
	log.Println(r.Proto, r.Method, r.Host, r.URL)
	if r.Method == "OPTIONS" {
		return
	}

	if len(r.Header.Get("Authorization")) == 0 {
		http.Error(w, "Empty authorization token", http.StatusUnauthorized)
		return
	}

	email, err := utils.ParseToken(r.Header.Get("Authorization"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	user := models.User{}
	err = storage.User().FindByEmail(email, &user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	post := models.Post{}
	err = readBodyFromParamsForPost(&post, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	post.User = user
	err = post.Validate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = storage.Post().Create(&post)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json, err := json.Marshal(post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(json)
}

func (p PostsController) Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setupResponse(w)
	log.Println(r.Proto, r.Method, r.Host, r.URL)

	if r.Method == "OPTIONS" {
		return
	}

	post := models.Post{}
	intID, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	post.Id = intID
	err = readBodyFromParamsForPost(&post, r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = post.Validate()

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = storage.Post().Update(&post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json, err := json.Marshal(post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(json)
}

func setupResponse(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func readBodyFromParamsForPost(post *models.Post, r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&post)
	if err != nil {
		return err
	}
	return nil
}
