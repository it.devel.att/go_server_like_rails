package utils

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/it.devel.att/gominds/models"
)

var MyAppKey []byte = []byte("secretString")

func CreateToken(user *models.User) (map[string]string, error) {
	type TokenPayload struct {
		Email string `json:"email"`
		jwt.StandardClaims
	}

	tokenPayload := TokenPayload{
		user.Email,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenPayload)
	tokenString, err := token.SignedString(MyAppKey)

	if err != nil {
		return nil, err
	}

	tokenJson := map[string]string{
		"token": tokenString,
	}

	return tokenJson, nil
}

func ParseToken(tokenString string) (string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return MyAppKey, nil
	})
	if err != nil {
		log.Println("jwt.Parse", err)
		return "", err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return fmt.Sprintf("%v", claims["email"]), nil
	} else {
		return "", errors.New("Invalid token")
	}

}
